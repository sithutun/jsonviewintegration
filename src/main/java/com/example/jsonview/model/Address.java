package com.example.jsonview.model;

import com.fasterxml.jackson.annotation.JsonView;

import java.io.Serializable;

public class Address implements Serializable{

    public interface View {

    }

    @JsonView(View.class)
    String no;

    @JsonView(View.class)
    String name;



    public Address() {
    }

    public Address(String no, String name) {
        this.no = no;
        this.name = name;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
