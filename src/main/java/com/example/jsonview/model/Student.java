package com.example.jsonview.model;

import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.ws.soap.addressing.server.annotation.Address;

import java.io.Serializable;

public class Student implements Serializable {

    public  interface View extends People.View{}

    @JsonView(View.class)
    String id;

    @JsonView(View.class)
    String grade;

    @JsonView(View.class)
    People people;

    public Student() {
    }

    public Student(String id, String grade) {
        this.id = id;
        this.grade = grade;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }


    public People getPeople() {
        return people;
    }

    public void setPeople(People people) {
        this.people = people;
    }
}
