package com.example.jsonview.model;

import com.fasterxml.jackson.annotation.JsonView;

import java.io.Serializable;


public class People implements Serializable{

    public interface View extends Address.View{

    }

    @JsonView(View.class)
    String name;

    String nationality;

    public People() {
    }

    public People(String name, String nationality) {
        this.name = name;
        this.nationality = nationality;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    @JsonView(View.class)
    Address address;

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
