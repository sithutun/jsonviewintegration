package com.example.jsonview.controller;

import com.example.jsonview.model.Address;
import com.example.jsonview.model.People;
import com.example.jsonview.model.Student;
import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Testing JsonView
 * See also <a href="https://spring.io/blog/2014/12/02/latest-jackson-integration-improvements-in-spring">JsonView</a>
 */
@Controller
public class JsonViewController {

//    @JsonView(People.View.class)
    @RequestMapping(value = "/")
    @ResponseBody
    public People getPeople(){

        Address address=new Address("11","Mandalay");
        People people=new People("STT","Myanmar");
        people.setAddress(address);
        return (people);


    }

//    @JsonView(Address.View.class)
    @RequestMapping(value = "/address")
    @ResponseBody
    public Address getAddress(){

        Address address=new Address("11","Mandalay");
        People people=new People("STT","Myanmar");
        people.setAddress(address);

        return address;


    }

    //@JsonView(Student.View.class)
    @RequestMapping(value = "/student")
    @ResponseBody
    public Student getStudent(){

        Address address=new Address("11","Mandalay");
        People people=new People("STT","Myanmar");
        people.setAddress(address);

        Student student=new Student("1","A");
        student.setPeople(people);
        return student;


    }
}
